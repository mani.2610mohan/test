import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import{ Pagetab1Component } from './pagetab1/pagetab1.component';
import { Pagetab2Component } from './pagetab2/pagetab2.component';
import { Tab3Component } from './tab3/tab3.component';
import { Tab4Component } from './tab4/tab4.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { from } from 'rxjs';



const routes: Routes = [
  { 
    path:'',
    component:Tab3Component
  },
  {
    path:'dashboard',
    component:DashboardComponent
  },
  {
    path:'pagetab1',
    component:Pagetab1Component
  
  },
  {
    path:'pagetab2',
    component:Pagetab2Component
  },
  
  {
    path:'tab4',
    component: Tab4Component
  },
  {
    path:'page2',
    component: Page2Component
  },
  {
    path:'page3',
    component: Page3Component
  }
];


 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

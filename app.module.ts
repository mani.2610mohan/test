import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Pagetab1Component } from './pagetab1/pagetab1.component';
import { Pagetab2Component } from './pagetab2/pagetab2.component';
import { Tab3Component } from './tab3/tab3.component';
import { Tab4Component } from './tab4/tab4.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    Pagetab1Component,
    Pagetab2Component,
    Tab3Component,
    Tab4Component,
    Page2Component,
    Page3Component,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


import { Router,ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, ControlContainer } from '@angular/forms';
import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'protractor';


@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.component.html',
  styleUrls: ['./tab3.component.css']
})
export class Tab3Component implements OnInit {
  form = new FormGroup({
    username : new FormControl('',Validators.required),
  password:new FormControl('',Validators.required)
  });

  constructor(private router: Router) { }
  
  


  ngOnInit(): void {
  }
submit() {
  alert(JSON.stringify(this.form.value));
    this.router.navigate(['/dashboard']);
  }
}
